# moxa-irigb-tools

Utility for controlling DA-IRIG-B expansion module
Compile and install the IRIG-B time sync daemon

1. Make the IRIG-B time sync daemon
```
apt update
apt install build-essential -y
make
```

2. Install the IRIG-B time sync daemon, ServiceSyncTime.
```
make install
```

3. Launch the ServiceSyncTime manually
```
ServiceSyncTime -t 1 -s 2 -i 10
```

4. Run the ServiceSyncTime automatically at booting

- For Debian system reference systemd service in package

Edit '/lib/systemd/system/mx_irigb.service':
```
[Unit]
Description=Moxa DA-IRIG-B daemon service

[Service]
Type=oneshot
ExecStart=/usr/sbin/mx_irigb.sh start
ExecStop=/usr/sbin/mx_irigb.sh stop
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

Enable service:
```
root@Moxa:/home/moxa# systemctl enable mx_irigb.service
Created symlink /etc/systemd/system/multi-user.target.wants/mx_irigb.service → /lib/systemd/system/mx_irigb.service.
root@Moxa:/home/moxa# systemctl status mx_irigb.service
● mx_irigb.service - Moxa DA-IRIG-B daemon service
   Loaded: loaded (/lib/systemd/system/mx_irigb.service; enabled; vendor preset: enabled)
   Active: active (exited) since Mon 2019-03-11 15:39:05 CST; 13min ago
  Process: 309 ExecStart=/usr/sbin/mx_irigb.sh start (code=exited, status=0/SUCCESS)
 Main PID: 309 (code=exited, status=0/SUCCESS)
    Tasks: 0 (limit: 4915)
   CGroup: /system.slice/mx_irigb.service

Mar 11 15:39:05 Moxa systemd[1]: Starting Moxa DA-IRIG-B daemon service...
Mar 11 15:39:05 Moxa systemd[1]: Started Moxa DA-IRIG-B daemon service.

```
For Ubuntu:
```
root@Moxa:/home/moxa/moxa-irigb-tools# cp -a fakeroot/etc/init.d/mx_irigb.sh /etc/init.d/
root@Moxa:/home/moxa/moxa-irigb-tools# update-rc.d mx_irigb.sh defaults
```
For Redhat Enterprise:
```
root@Moxa:/home/moxa/moxa-irigb-tools# chkconfig --levels 2345 mx_irigb.sh on
```
