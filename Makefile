CXX=g++
DESTDIR ?= /

DIR = mxirig \
mxIrigUtil \
mxSyncTimeSvc

all:
	for i in $(DIR); do \
		make -C $$i; \
	done

install:
	/usr/bin/install -d $(DESTDIR)/sbin
	/usr/bin/install mx_irigb.sh $(DESTDIR)/sbin/mx_irigb.sh
	/usr/bin/install mxSyncTimeSvc/ServiceSyncTime $(DESTDIR)/sbin/ServiceSyncTime
	/usr/bin/install mxIrigUtil/mxIrigUtil $(DESTDIR)/sbin/mxIrigUtil

clean:
	for i in $(DIR); do \
		make -C $$i clean; \
	done
